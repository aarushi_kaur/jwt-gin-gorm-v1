package main

import (
	//"os"

	"github.com/aarushikaur/user-login-signup/controllers"
	"github.com/aarushikaur/user-login-signup/database"
	"github.com/aarushikaur/user-login-signup/middlewares"

	//"user-login-signup/middlewares"
	"github.com/gin-gonic/gin"
)

func main() {
	// config := &database.Config{
	// 	Host:     os.Getenv("DB_HOST"),
	// 	Port:     os.Getenv("DB_PORT"),
	// 	Password: os.Getenv("DB_PASS"),
	// 	User:     os.Getenv("DB_USER"),
	// 	SSLMode:  os.Getenv("DB_SSLMODE"),
	// 	DBName:   os.Getenv("DB_NAME"),
	// }
	database.NewConnection()
	// database.AutoMigrate()

	router := initRouter()
	router.Run(":8080")
}
func initRouter() *gin.Engine {
	router := gin.Default()
	api := router.Group("/api")
	{
		api.POST("/token", controllers.GenerateToken)
		api.POST("/user/register", controllers.RegisterUser)
		secured := api.Group("/secured").Use(middlewares.Auth())
		{
			secured.GET("/ping", controllers.Ping)
		}
	}
	return router
}
