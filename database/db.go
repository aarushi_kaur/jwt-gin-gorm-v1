package database

import (
	"fmt"
	"log"
	"os"

	"github.com/joho/godotenv"
	"gorm.io/driver/postgres"
	"gorm.io/gorm"
)

type Config struct {
	Host     string
	Port     string
	Password string
	User     string
	DBName   string
	SSLMode  string
}

//var Instance *gorm.DB

// func NewConnection(config *Config) (*gorm.DB, error) {

// 	err := godotenv.Load(".env")

// 	if err != nil {
// 		log.Fatalf("Error loading .env file")
// 	}

// 	envMap, mapErr := godotenv.Read(".env")
// 	if mapErr != nil {
// 		fmt.Printf("Error loading .env into map[string]string\n")
// 		os.Exit(1)
// 	}

// 	dbUser := envMap["DB_USER"]
// 	dbPassword := envMap["DB_PASS"]
// 	dbName := envMap["DB_NAME"]
// 	 dbHost := envMap["DB_HOST"]
// 	// dbPort := envMap["DB_PORT"]
// 	dbSSLMode := envMap["DB_SSLMODE"]

// 	dsn := fmt.Sprintf(
// 		"user=%s  password=%s dbname=%s sslmode=%s",
// 		dbUser, dbPassword, dbName, dbSSLMode,
// 	)
// 	fmt.Println("database url : ", dsn)
// 	fmt.Println("host is ", config.Host:=dbHost)

// 	db, err := gorm.Open(postgres.Open(dsn), &gorm.Config{})
// 	//db,err:=gorm.Open(dbUser,dsn)
// 	if err != nil {
// 		return db, err
// 	}
// 	return db, nil

// }

// func Migrate() {
// 	Instance.AutoMigrate(&models.User{})
// 	log.Println("Database Migration Completed!")
// }

//var DB *gorm.DB
// func ConnectDataBase() {
//     err := godotenv.Load(".env")
//     if err != nil {
//         log.Fatalf("Error loading .env file")
//     }
//     Dbdriver := os.Getenv("DB_DRIVER")
//     DbHost := os.Getenv("DB_HOST")
//     DbUser := os.Getenv("DB_USER")
//     DbPassword := os.Getenv("DB_PASSWORD")
//     DbName := os.Getenv("DB_NAME")
//     DbPort := os.Getenv("DB_PORT")
//     DBURL := fmt.Sprintf("%s:%s@tcp(%s:%s)/%s?charset=utf8&parseTime=True&loc=Local", DbUser, DbPassword, DbHost, DbPort, DbName)
//     Instance, err = gorm.Open(Dbdriver, DBURL)
//     if err != nil {
//         fmt.Println("Cannot connect to database ", Dbdriver)
//         log.Fatal("connection error:", err)
//     } else {
//         fmt.Println("We are connected to the database ", Dbdriver)
//     }
//     Instance.AutoMigrate(&User{})
// }

var DB *gorm.DB

//Connecting Database
func NewConnection() {
	err := godotenv.Load(".env")

	if err != nil {
		log.Fatalf("Error loading .env file")
	}

	//Getting Database Credentials from .env file
	DbUser := os.Getenv("DB_USER")
	DbPassword := os.Getenv("DB_PASSWORD")
	DbName := os.Getenv("DB_NAME")

	//Url
	dsn := fmt.Sprintf("user=%s password=%s dbname=%s sslmode=disable", DbUser, DbPassword, DbName)

	DB, err := gorm.Open(postgres.Open(dsn), &gorm.Config{})

	if err != nil {
		fmt.Println("Cannot connect to database ")
		log.Fatalln("Connection error: ", err)
	} else {
		fmt.Println("We are connected to the database!!")
	}

	//Migrating data into db
	DB.AutoMigrate(&User{})
}
